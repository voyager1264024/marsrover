class Rover {
    constructor(xPosition, yPosition, direction, plateau) {        
        const directions = ['N', 'W', 'S', 'E'];
        if (!directions.includes(direction))
            throw new Error(`Wrong rover direction.`);

        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.direction = direction;
        this.plateau = plateau;
    }
  
    executeInstructions(instructions) {
        for (let instruction of instructions) {
            switch (instruction) {
                case 'L':
                    this.rotateCounterClockwise();
                    break;

                case 'R':
                    this.rotateClockwise();
                    break;

                case 'M':
                    this.move();
                    break;

                default:
                    throw new Error(`Wrong moviment instruction. Final Position: ${this.getPosition()}`);
            }
        }
    }
  
    rotateCounterClockwise() {
        const directions = ['N', 'W', 'S', 'E'];
        this.direction = directions[(directions.indexOf(this.direction) + 1) % 4];
    }
  
    rotateClockwise() {
        const directions = ['N', 'E', 'S', 'W'];
        this.direction = directions[(directions.indexOf(this.direction) + 1) % 4];
    }
  
    move() {
        let { xPosition, yPosition } = this;
        switch(this.direction) {
            case 'N':
                yPosition += 1;
                break;

            case 'S':
                yPosition -= 1;
                break;

            case 'E':
                xPosition += 1;
                break;

            case 'W':
                xPosition -= 1;
                break;
        }
    
        if (this.plateau.isWithinBounds(xPosition, yPosition)) {
            this.xPosition = xPosition;
            this.yPosition = yPosition;
        }
    }
  
    getPosition() {
        return `${this.xPosition} ${this.yPosition} ${this.direction}`;
    }
}
  
module.exports = Rover;
  