const Plateau = require('../src/plateau');
const Rover = require('../src/rover');

test('Rover movement and rotation: ', () => {
    const plateau = new Plateau(5, 5);

    const rover = new Rover(1, 2, 'N', plateau);
    rover.executeInstructions('LMLMLMLMM');
    expect(rover.getPosition()).toBe('1 3 N');

    const rover2 = new Rover(3, 3, 'E', plateau);
    rover2.executeInstructions('MRRMMRMRRM');
    expect(rover2.getPosition()).toBe('2 3 S');
});