class Plateau {
    constructor(upperRightX, upperRightY) {
        this.upperRightX = upperRightX;
        this.upperRightY = upperRightY;
    }
  
    isWithinBounds(x, y) {
        return x >= 0 && x <= this.upperRightX && y >= 0 && y <= this.upperRightY;
    }
}

module.exports = Plateau;
