const readline = require('readline');
const Plateau = require('./plateau');
const Rover = require('./rover');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let plateau;
const rovers = [];

const askQuestion = (question) => {
    return new Promise((resolve) => rl.question(question, resolve));
};

async function validateRoverPosition(input) {
    const regex = /^\d+\s\d+\s[NESW]$/;
    if (!regex.test(input))
        throw new Error('Invalid rover position format. Please enter in the format "1 2 N".');
}

async function validatePlateau(input) {
    const regex = /^\d+\s\d+$/;
    if (!regex.test(input))
        throw new Error('Invalid plateau format. Please enter in the format "5 5".');
}

const main = async () => {
    const plateauCoordinates = await askQuestion('Enter the upper-right coordinates of the plateau (e.g., "5 5"): ');
    validatePlateau(plateauCoordinates);
    const [upperRightX, upperRightY] = plateauCoordinates.split(' ').map(Number);
    plateau = new Plateau(upperRightX, upperRightY);

    while (true) {
        let roverPosition;
        if (!rovers.some(rover => rover instanceof Rover))
            roverPosition = await askQuestion('Enter the rover position and direction (e.g., "1 2 N"): ');
        else
            roverPosition = await askQuestion('Enter the rover position and direction (e.g., "1 2 N"). If there is no more rover, enter \'exit\': ');
        
        if (roverPosition.toLowerCase() === 'exit') 
            break;
        await validateRoverPosition(roverPosition);

        const [xPosition, yPosition, direction] = roverPosition.split(' ');
        const rover = new Rover(Number(xPosition), Number(yPosition), direction, plateau);
        rovers.push(rover);

        const instructions = await askQuestion('Enter the movement instructions (e.g., "LMLMLMLMM"): ');
        rover.executeInstructions(instructions);

        console.log('Final Position:', rover.getPosition());
    }

    rl.close();
};

main();
