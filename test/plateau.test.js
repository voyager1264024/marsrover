const Plateau = require('../src/plateau');

test('Plateau within bounds', () => {
    const plateau = new Plateau(5, 5);
    expect(plateau.isWithinBounds(3, 3)).toBe(true);
    expect(plateau.isWithinBounds(5, 5)).toBe(true);
    expect(plateau.isWithinBounds(5, 6)).toBe(false);
    expect(plateau.isWithinBounds(6, 5)).toBe(false);
})