# Mars Rover


## Getting started

This application simulates the movement of rovers on the surface of Mars within a defined plateau area. Users can input the size of the plateau and the initial position and movement instructions for each rover.


## Features

- Plateau Initialization: Users are prompted to input the upper-right coordinates of the plateau, defining the size of the area where rovers will navigate.

- Rover Deployment: The application allows users to deploy multiple rovers onto the plateau. Users can specify the initial position and direction (N, E, S, or W) of each rover.

- Movement Instructions: Once deployed, users can provide movement instructions for each rover. Instructions include turning left (L), turning right (R), and moving forward (M).

- Validation: The application validates user inputs to ensure they adhere to the specified formats. This includes verifying the format of the plateau coordinates and rover positions.


## Example

Enter the upper-right coordinates of the plateau (e.g., "5 5"): 5 5  
Enter the rover position and direction (e.g., "1 2 N"): 1 2 N  
Enter the movement instructions (e.g., "LMLMLMLMM"): LMLMLMLMM  
Final Position: 1 3 N  

Enter the rover position and direction (e.g., "1 2 N"). If there is no more rover, enter 'exit': 3 3 E  
Enter the movement instructions (e.g., "LMLMLMLMM"): MMRMMRMRRM  
Final Position: 5 1 E  

Enter the rover position and direction (e.g., "1 2 N"). If there is no more rover, enter 'exit': exit  


## Notes

- The application assumes valid inputs from users and does not handle errors beyond format validation.

- Each rover's movement is constrained by the boundaries of the plateau. If a rover attempts to move beyond the plateau's edges, it will remain stationary.


## Running

To run the code, follow these instructions in your terminal:
- npm install
- npm init -y
- npm test (to run the automated tests)
- node src/index.js (to run the index code)

Ensure you are in the root directory of the project when executing these commands. Feel free to reach out if you encounter any issues.
